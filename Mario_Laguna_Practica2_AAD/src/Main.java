import gui.Controlador;
import gui.Modelo;
import gui.Vista;

public class Main {

    /**
     * Metodo main para ejecutar el programa
     * @param args
     */
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
