package gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.time.format.DateTimeFormatter;

public class Vista {
    JTextField txtIsbn;
    JTextField txtTitulo;
    JTextField txtEditorial;
    JTextField txtAutor;
    JTextField txtBuscar;
    JButton btnBuscar;
    JButton btnNuevo;
    JButton btnEliminar;
    JTable tabla;
    DateTimePicker dpFecha;
    private JPanel panel;
    JButton btnModificar;
    JButton btnOrdenarIsbn;
    JButton btnOrdenarAutor;
    DefaultTableModel dtmTabla;
    JMenuItem conectar;
    JMenuItem salir;
    JMenuItem table;
    JFrame frame;

    /**
     * Constructor donde creo y le doy propiedades a la vista
     */
    public Vista(){
        frame = new JFrame("Mario Laguna");
        frame.setContentPane(panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        dtmTabla = new DefaultTableModel();
        tabla.setModel(dtmTabla);
        crearMenu();
        frame.pack();
        frame.setVisible(true);
    }

    /**
     * Metodo para crear un JMenuBar
     */
    private void crearMenu(){
        conectar = new JMenuItem("Conectar");
        conectar.setActionCommand("Conectar");
        salir = new JMenuItem("Salir");
        salir.setActionCommand("Salir");
        table = new JMenuItem("Crear tabla");
        table.setActionCommand("table");
        JMenu archivo = new JMenu("Archivo");
        archivo.add(conectar);
        archivo.add(salir);
        archivo.add(table);
        JMenuBar barra = new JMenuBar();
        barra.add(archivo);
        frame.setJMenuBar(barra);
    }

}
