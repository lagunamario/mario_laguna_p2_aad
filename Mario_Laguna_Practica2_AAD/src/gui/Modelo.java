package gui;

import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class Modelo {
    private Connection conexion;

    /**
     * Constructor vacio del modelo
     */
    public Modelo(){

    }

    /**
     * Metodo para que conecte con la base de datos de MySQL
     * @throws SQLException
     */
    public void conectar() throws SQLException {
        conexion = null;
        conexion = DriverManager.getConnection("jdbc:mysql://localhost:3306/tienda_libros", "root", "");
        JOptionPane.showMessageDialog(null, "Conectado");
    }

    /**
     * Metodo para desconectar de la base
     * @throws SQLException
     */
    public void desconectar() throws SQLException {
        conexion.close();
        conexion = null;
    }

    /**
     * Metodo para buscar por isbn
     * @param isbn
     * @return
     * @throws SQLException
     */
    public ResultSet busqueda(String isbn) throws SQLException {
        String consulta = "SELECT * from libros WHERE isbn like " + "\'" + isbn + "\'" ;
        PreparedStatement sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * MEtodo para obtener los datos de la tabla desde Mysql
     * @return
     * @throws SQLException
     */
    public ResultSet obtenerDatos() throws SQLException {
        if (conexion == null){
            return null;
        }
        if (conexion.isClosed()){
            return null;
        }
        String consulta = "SELECT * FROM libros";
        PreparedStatement sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo para ordenar por isbn
     * @return
     * @throws SQLException
     */
    public ResultSet ordenarISBN() throws SQLException {
        String consulta = "SELECT * FROM libros ORDER BY isbn";
        PreparedStatement sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * MEtodo para ordenar por autor
     * @return
     * @throws SQLException
     */
    public ResultSet ordenarAutor() throws SQLException {
        String consulta = "SELECT * FROM libros ORDER BY autor";
        PreparedStatement sentencia = conexion.prepareStatement(consulta);
        ResultSet resultado = sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo para insertar un nuevo libro
     * @param isbn
     * @param titulo
     * @param editorial
     * @param autor
     * @param fechaVenta
     * @return
     * @throws SQLException
     */
    public int insertarLibro(String isbn, String titulo, String editorial, String autor, LocalDateTime fechaVenta) throws SQLException {
        if (conexion == null){
            return -1;
        }
        if (conexion.isClosed()){
            return -2;
        }
        String consulta = "INSERT INTO libros(isbn, titulo, editorial, autor, fecha_venta) values (?,?,?,?,?)";
        PreparedStatement sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, isbn);
        sentencia.setString(2, titulo);
        sentencia.setString(3, editorial);
        sentencia.setString(4, autor);
        sentencia.setTimestamp(5, Timestamp.valueOf(fechaVenta));

        int numerosRegistros = sentencia.executeUpdate();
        if (sentencia != null){
            sentencia.close();
        }
        return numerosRegistros;
    }

    /**
     * Metodo para crear la tabla de libros en caso de que no exista en mysql
     * @throws SQLException
     */
    public void crearTablaLibros() throws SQLException {
        String sql = "CREATE TABLE libros (id int primary key auto_increment unique,\n" +
                "isbn varchar(13) unique,\n" +
                "titulo varchar(25),\n" +
                "editorial varchar(30),\n" +
                "autor varchar(25),\n" +
                "fecha_venta timestamp\n" +
                ");";
        PreparedStatement sentencia = conexion.prepareStatement(sql);
        sentencia.executeUpdate();

    }

    /**
     * Metodo para eliminar un libro
     * @param id
     * @return
     * @throws SQLException
     */
    public int eliminarLibro(int id) throws SQLException {
        if (conexion == null){
            return -1;
        }
        if (conexion.isClosed()){
            return -2;
        }
        String consulta = "DELETE FROM libros WHERE id = ?";
        PreparedStatement sentencia = null;
        sentencia = conexion.prepareStatement(consulta);
        sentencia.setInt(1, id);
        int resultado = sentencia.executeUpdate();
        if (sentencia != null){
            sentencia.close();
        }
        return resultado;
    }

    /**
     * Metodo para modificar un libro
     * @param id
     * @param isbn
     * @param titulo
     * @param editorial
     * @param autor
     * @param fechaVenta
     * @return
     * @throws SQLException
     */
    public int modificarLibro(int id, String isbn, String titulo, String editorial, String autor, Timestamp fechaVenta) throws SQLException {
        if (conexion == null){
            return -1;
        }
        if (conexion.isClosed()){
            return -2;
        }
        String consulta = "UPDATE libros SET isbn = ?, titulo = ?, editorial = ?, autor = ?, fecha_venta = ? where id = ?";
        PreparedStatement sentencia = conexion.prepareStatement(consulta);
        sentencia.setString(1, isbn);
        sentencia.setString(2, titulo);
        sentencia.setString(3, editorial);
        sentencia.setString(4, autor);
        sentencia.setTimestamp(5, fechaVenta);
        sentencia.setInt(6, id);
        int resultado = sentencia.executeUpdate();
        if (sentencia == null){
            sentencia.close();
        }
        return resultado;
    }

}
