package gui;

import com.mysql.jdbc.exceptions.jdbc4.MySQLSyntaxErrorException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableRowSorter;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Controlador implements ActionListener, TableModelListener, /*ListSelectionListener*/MouseListener {
    Vista vista;
    Modelo modelo;

    private enum tipoEstado {conectado, desconectado};
    private tipoEstado estado;

    /**
     * Constructor de controlador donde se pasa la vista y modelo
     * @param vista
     * @param modelo
     */
    public Controlador(Vista vista, Modelo modelo){
        this.vista = vista;
        this.modelo = modelo;
        this.estado = tipoEstado.desconectado;
        iniciarTabla();
        addActionListener(this);
        addTableModelListener(this);
        //addListSelectionListener(this);
        addMouseListener(this);
    }

    /**
     * Metodo para darle nombres a las columnas del JTable
     */
    private void iniciarTabla(){
        String[] header = {"ID", "ISBN", "TITULO", "AUTOR", "EDITORIAL", "FECHA DE VENTA"};
        vista.dtmTabla.setColumnIdentifiers(header);
    }

    /**
     * MEtodo para anadir todos los actionListener de golpe
     * @param listener
     */
    private void addActionListener(ActionListener listener){
        vista.btnNuevo.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.btnBuscar.addActionListener(listener);
        vista.conectar.addActionListener(listener);
        vista.salir.addActionListener(listener);
        vista.btnModificar.addActionListener(listener);
        vista.table.addActionListener(listener);
        vista.btnOrdenarIsbn.addActionListener(listener);
        vista.btnOrdenarAutor.addActionListener(listener);
    }

    /**
     * Metodo para anadir los tableModelListener
     * @param listener
     */
    private void addTableModelListener(TableModelListener listener){
        vista.dtmTabla.addTableModelListener(listener);
    }

    /**
     * Metodo para anadir el mouseListener
     * @param listener
     */
    private void addMouseListener(MouseListener listener){
        vista.tabla.addMouseListener(listener);
    }

    /**
     * Metodo para anadir los listSelectionListener
     * @param listener
     */
    private void addListSelectionListener(ListSelectionListener listener){
        vista.tabla.getSelectionModel().addListSelectionListener(listener);
    }

    /**
     * MEtodo para que se carguen las filas del mysql a la JTable
     * @param resultSet
     * @throws SQLException
     */
    public void cargarFilas(ResultSet resultSet) throws SQLException {
        Object[] fila = new Object[6];
        vista.dtmTabla.setRowCount(0);
        while(resultSet.next()) {
            fila[0] = resultSet.getObject("ID");
            fila[1] = resultSet.getObject("ISBN");
            fila[2] = resultSet.getObject("TITULO");
            fila[3] = resultSet.getObject("AUTOR");
            fila[4] = resultSet.getObject("EDITORIAL");
            fila[5] = resultSet.getObject("fecha_venta");
            vista.dtmTabla.addRow(fila);
        }
        if (resultSet.last()){
            JOptionPane.showMessageDialog(null, "Filas cargadas");
        }
    }

    /**
     * MEtodo obligatorio del TableModelSelection donde modifica en caso de que detecte un cambio
     * @param e
     */
    @Override
    public void tableChanged(TableModelEvent e) {
        if (e.getType() == TableModelEvent.UPDATE){
            System.out.println("Actualizada");
            int filaModificada = e.getFirstRow();
            try{
                Date fecha_venta;
                modelo.modificarLibro((Integer) vista.dtmTabla.getValueAt(filaModificada, 0),
                        (String) vista.dtmTabla.getValueAt(filaModificada, 1),
                        (String) vista.dtmTabla.getValueAt(filaModificada, 2),
                        (String) vista.dtmTabla.getValueAt(filaModificada, 3),
                        (String) vista.dtmTabla.getValueAt(filaModificada, 4),
                        //Date fecha_venta = (Date) vista.dtmTabla.getValueAt(vista.tabla.getSelectedRow(), 5);
                        //vista.dpFecha.setDate(fecha_venta.toLocalDate());
                        (Timestamp) vista.dtmTabla.getValueAt(filaModificada, 5));
            }catch (SQLException ex){
                ex.printStackTrace();
            }
        }
    }

    /**
     * Metodo obligatorio de actionListener que usa un switch haciendo distintas funciones dependiendo del actionCommand
     * de los botones
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        switch (comando){
            case "Nuevo":
                try{
                    modelo.insertarLibro(vista.txtIsbn.getText(), vista.txtTitulo.getText(), vista.txtAutor.getText(),
                            vista.txtEditorial.getText(), vista.dpFecha.getDateTimePermissive());
                    cargarFilas(modelo.obtenerDatos());
                    limpiarCampos();
                } catch(SQLException ex){
                    ex.printStackTrace();
                }
                break;
            case "Buscar":
                String valorBusqueda = vista.txtBuscar.getText();
                if (vista.txtBuscar.getText().length() >= 2){
                    try {
                        cargarFilas(modelo.busqueda(valorBusqueda));
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            case "table":
                try {
                    modelo.crearTablaLibros();
                    JOptionPane.showMessageDialog(null,"Tabla libros creada");
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "ordenarIsbn":
                try {
                    cargarFilas(modelo.ordenarISBN());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "ordenarAutor":
                try {
                    cargarFilas(modelo.ordenarAutor());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Eliminar":
                int filaBorrar = vista.tabla.getSelectedRow();
                int idBorrar = (Integer) vista.dtmTabla.getValueAt(filaBorrar, 0);
                try{
                    modelo.eliminarLibro(idBorrar);
                    vista.dtmTabla.removeRow(filaBorrar);
                }catch(SQLException ex){
                    ex.printStackTrace();
                }
                break;
            case "Modificar":
                int filaModificar = vista.tabla.getSelectedRow();
                int idModificar = (int) vista.dtmTabla.getValueAt(filaModificar, 0);
                /*String isbnModificar = (String) vista.dtmTabla.getValueAt(filaModificar, 1);
                String tituloModificar = (String) vista.dtmTabla.getValueAt(filaModificar, 2);
                String editorialModificar = (String) vista.dtmTabla.getValueAt(filaModificar, 3);
                String autorModificar = (String) vista.dtmTabla.getValueAt(filaModificar, 4);
                LocalDate fechaVentaModificar = (LocalDate) vista.dtmTabla.getValueAt(filaModificar, 5);*/
                String isbnModificar = vista.txtIsbn.getText();
                String tituloModificar = vista.txtTitulo.getText();
                String editorialModificar = vista.txtEditorial.getText();
                String autorModificar = vista.txtAutor.getText();
                Timestamp fechaVentaModificar = Timestamp.valueOf(vista.dpFecha.getDateTimePermissive());
                try {
                    modelo.modificarLibro(idModificar, isbnModificar, tituloModificar, editorialModificar, autorModificar, fechaVentaModificar);
                    cargarFilas(modelo.obtenerDatos());
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Salir":
                System.exit(0);
                break;
            case "Conectar":
                if (estado == tipoEstado.desconectado){
                    try{
                        modelo.conectar();
                    }catch (SQLException ex){
                        System.out.println("No hay tabla");
                    }
                    vista.conectar.setText("Desconectar");
                    estado = tipoEstado.conectado;
                    try{
                        cargarFilas(modelo.obtenerDatos());
                    }catch (SQLException ex){
                        System.out.println("No hay tabla");
                    }
                }
                else{
                    try{
                        modelo.desconectar();
                    }catch (SQLException ex){
                        ex.printStackTrace();
                    }
                    vista.conectar.setText("Conectar");
                    estado = tipoEstado.desconectado;
                    JOptionPane.showMessageDialog(null, "Desconectado");
                }
                break;
        }
    }

    /**
     * Metodo para limpiar los campos de texto
     */
    private void limpiarCampos() {
        vista.txtIsbn.setText("");
        vista.txtTitulo.setText("");
        vista.txtEditorial.setText("");
        vista.txtAutor.setText("");
        vista.dpFecha.setDateTimePermissive(null);
    }

    /*@Override
    public void valueChanged(ListSelectionEvent e) {
        int filaMostrar = vista.tabla.getSelectedRow();
        String isbn = (String) vista.dtmTabla.getValueAt(filaMostrar, 1);
        vista.txtIsbn.setText(isbn);
        String titulo = (String) vista.dtmTabla.getValueAt(filaMostrar, 2);
        vista.txtTitulo.setText(titulo);
        String editorial = (String) vista.dtmTabla.getValueAt(filaMostrar, 3);
        vista.txtEditorial.setText(editorial);
        String autor = (String) vista.dtmTabla.getValueAt(filaMostrar, 4);
        vista.txtAutor.setText(autor);
        LocalDateTime fecha = (LocalDateTime) vista.dtmTabla.getValueAt(filaMostrar, 5);
        vista.dpFecha.setDateTimePermissive(fecha);
    }*/

    /**
     * Metodo obligatorio del mouseListener cuando detecta un click
     * @param e
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        int filaModificada = vista.tabla.getSelectedRow();
        String isbn = (String) vista.dtmTabla.getValueAt(filaModificada, 1);
        vista.txtIsbn.setText(isbn);
        String titulo = (String) vista.dtmTabla.getValueAt(filaModificada, 2);
        vista.txtTitulo.setText(titulo);
        String editorial = (String) vista.dtmTabla.getValueAt(filaModificada, 3);
        vista.txtEditorial.setText(editorial);
        String autor = (String) vista.dtmTabla.getValueAt(filaModificada, 4);
        vista.txtAutor.setText(autor);
        LocalDate fecha = (LocalDate) vista.dtmTabla.getValueAt(filaModificada, 5);
        vista.dpFecha.setDateTimePermissive(LocalDateTime.from(fecha));
    }

    /**
     * Metodo obligatorio sin uso del mouseListener cuando detecta que presionas el raton
     * @param e
     */
    @Override
    public void mousePressed(MouseEvent e) {

    }

    /**
     * Metodo obligatorio sin uso del mouseListener cuando detecta que liberas el boton
     * @param e
     */
    @Override
    public void mouseReleased(MouseEvent e) {

    }

    /**
     * Metodo obligatorio sin uso del mouseListener cuando detecta que el cursor entra en un elemento
     * @param e
     */
    @Override
    public void mouseEntered(MouseEvent e) {

    }

    /**
     * Metodo obligatorio sin uso del mouseListener cuando detecta que el cursor sale de un elemento
     * @param e
     */
    @Override
    public void mouseExited(MouseEvent e) {

    }
}
